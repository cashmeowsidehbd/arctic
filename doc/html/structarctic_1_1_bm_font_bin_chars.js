var structarctic_1_1_bm_font_bin_chars =
[
    [ "Log", "structarctic_1_1_bm_font_bin_chars.html#a477d8fe2fc72cc6103ab6d0ae868d3ec", null ],
    [ "chnl", "structarctic_1_1_bm_font_bin_chars.html#a5ee1121bf588a760c4b93dfa9fc82d28", null ],
    [ "height", "structarctic_1_1_bm_font_bin_chars.html#a4514d333983c8069e87e00161be1ea34", null ],
    [ "id", "structarctic_1_1_bm_font_bin_chars.html#abcde8fafeaee264e891e447ad7ecc299", null ],
    [ "page", "structarctic_1_1_bm_font_bin_chars.html#aefc618acf841da46114026f22e56d528", null ],
    [ "width", "structarctic_1_1_bm_font_bin_chars.html#a9dd713a79b068e99b14aebf74049fb81", null ],
    [ "x", "structarctic_1_1_bm_font_bin_chars.html#a574e6e54ca1dbdb8c451842ecb824428", null ],
    [ "xadvance", "structarctic_1_1_bm_font_bin_chars.html#a5a490bac83687f6722cc26be9aafe404", null ],
    [ "xoffset", "structarctic_1_1_bm_font_bin_chars.html#ae1ced97593d8b1ff258a7182a766fc7c", null ],
    [ "y", "structarctic_1_1_bm_font_bin_chars.html#afa1d0c2d89c2c71f3d5b0b7b7813192e", null ],
    [ "yoffset", "structarctic_1_1_bm_font_bin_chars.html#a9b3073ba47a583b1e81f6b2955ae17c5", null ]
];