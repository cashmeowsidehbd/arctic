var structarctic_1_1ofbx_1_1_texture =
[
    [ "TextureType", "structarctic_1_1ofbx_1_1_texture.html#a796d2e69fd9c7e34802090c07d5cfce6", [
      [ "DIFFUSE", "structarctic_1_1ofbx_1_1_texture.html#a796d2e69fd9c7e34802090c07d5cfce6a3096490da2c1ebfb09a17c6f56c8d28e", null ],
      [ "NORMAL", "structarctic_1_1ofbx_1_1_texture.html#a796d2e69fd9c7e34802090c07d5cfce6ac6d6e24d54844352b5a20aea297a7045", null ],
      [ "COUNT", "structarctic_1_1ofbx_1_1_texture.html#a796d2e69fd9c7e34802090c07d5cfce6a07e931b332b1bd0cefa7330eef2da097", null ]
    ] ],
    [ "Texture", "structarctic_1_1ofbx_1_1_texture.html#a162aa1573e1a53163c4dfc6865edbae8", null ],
    [ "getFileName", "structarctic_1_1ofbx_1_1_texture.html#ae7513648152339bcebd2b96f98f158fc", null ],
    [ "getRelativeFileName", "structarctic_1_1ofbx_1_1_texture.html#a9401d50da405b36ef2689d93881e8009", null ]
];