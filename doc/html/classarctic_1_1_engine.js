var classarctic_1_1_engine =
[
    [ "Draw2d", "classarctic_1_1_engine.html#a841c5146e15ee79613bc7a3b6e5e622e", null ],
    [ "GetBackbuffer", "classarctic_1_1_engine.html#ae5db8109a38206e7ee0b65266bf7cd0d", null ],
    [ "GetMathTables", "classarctic_1_1_engine.html#a0d140bcbdf2856e3f02b41ef37faee5f", null ],
    [ "GetRandom", "classarctic_1_1_engine.html#affa6084795221b9dec00c9a543cf759c", null ],
    [ "GetTime", "classarctic_1_1_engine.html#ac702496898818fec7c2c3a540c239792", null ],
    [ "GetWindowSize", "classarctic_1_1_engine.html#a0392f1d1e2a334a1886c2e93675f52e5", null ],
    [ "Init", "classarctic_1_1_engine.html#af5992ab22ba5d94c192d925885a0d00d", null ],
    [ "MouseToBackbuffer", "classarctic_1_1_engine.html#a428d85d5762d92093279e9fd7abb480f", null ],
    [ "OnWindowResize", "classarctic_1_1_engine.html#affff01fa7f0477cf1d703ac9de0ee0e6", null ],
    [ "ResizeBackbuffer", "classarctic_1_1_engine.html#adcd146a9e4f6ebdfd6a5f553a5a19b85", null ],
    [ "SetInverseY", "classarctic_1_1_engine.html#a2b142fc4cc503ba5c33b2b93aa84836d", null ]
];