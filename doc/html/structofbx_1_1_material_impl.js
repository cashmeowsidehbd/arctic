var structofbx_1_1_material_impl =
[
    [ "MaterialImpl", "structofbx_1_1_material_impl.html#a64e3d593cb35f9d12a66a99b1c8c9a36", null ],
    [ "getDiffuseColor", "structofbx_1_1_material_impl.html#aefc158a4a1adeee98175a68e7424ec65", null ],
    [ "getTexture", "structofbx_1_1_material_impl.html#ac0e731654ea28e941cbe67b027b8083b", null ],
    [ "getType", "structofbx_1_1_material_impl.html#a68d4fd367fd585a98b51b262fc318b0c", null ],
    [ "diffuse_color", "structofbx_1_1_material_impl.html#aa8e767fba5037fd2eb43507512afe78f", null ],
    [ "textures", "structofbx_1_1_material_impl.html#ac2d79cd0de390f9161480454254c5551", null ]
];