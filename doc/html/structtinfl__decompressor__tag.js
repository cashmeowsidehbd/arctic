var structtinfl__decompressor__tag =
[
    [ "m_bit_buf", "structtinfl__decompressor__tag.html#a534e77162cb88e3d5b2ca52378860e04", null ],
    [ "m_check_adler32", "structtinfl__decompressor__tag.html#a80c6f22b32fb288a5d376f6f93b2d63d", null ],
    [ "m_counter", "structtinfl__decompressor__tag.html#a55d43ca0603c01e6b40945c901e1343e", null ],
    [ "m_dist", "structtinfl__decompressor__tag.html#ad0221a26f3135d362943de2e2806aa12", null ],
    [ "m_dist_from_out_buf_start", "structtinfl__decompressor__tag.html#a7f3575865115ab117d1d16afd5c173d9", null ],
    [ "m_final", "structtinfl__decompressor__tag.html#a39ed69be124315c9b42358d27b285987", null ],
    [ "m_len_codes", "structtinfl__decompressor__tag.html#adb33b4be77767c95e8b1816567583ba1", null ],
    [ "m_num_bits", "structtinfl__decompressor__tag.html#aa27988907a00d70bbeb36f4c4c09a2cf", null ],
    [ "m_num_extra", "structtinfl__decompressor__tag.html#af646c16845f58dd81ee8dc03e0fc2a43", null ],
    [ "m_raw_header", "structtinfl__decompressor__tag.html#a9e1f3aec5562c9b6fe92d96ad70b6d29", null ],
    [ "m_state", "structtinfl__decompressor__tag.html#a92296a332b76f047a4cd7ad8437c4735", null ],
    [ "m_table_sizes", "structtinfl__decompressor__tag.html#ac592ab5540fb0bd0ae64867ed50402b2", null ],
    [ "m_tables", "structtinfl__decompressor__tag.html#a98139fb01bb5d503cb24acbd23af33a1", null ],
    [ "m_type", "structtinfl__decompressor__tag.html#a25a2446091964983dc8a4b01064a287c", null ],
    [ "m_z_adler32", "structtinfl__decompressor__tag.html#aab03536da4b49c3125b11f8c7e999895", null ],
    [ "m_zhdr0", "structtinfl__decompressor__tag.html#a3f27466b6f5ec49b73db82fde43118c3", null ],
    [ "m_zhdr1", "structtinfl__decompressor__tag.html#aa20c2fae45d99a1ae97fb7d58fefcd48", null ]
];