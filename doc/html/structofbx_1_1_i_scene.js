var structofbx_1_1_i_scene =
[
    [ "~IScene", "structofbx_1_1_i_scene.html#aee7b536af693f0ab0255fac2703e16a4", null ],
    [ "destroy", "structofbx_1_1_i_scene.html#a4b639a59674f2d7a2ea2e41d0ed83952", null ],
    [ "getAllObjectCount", "structofbx_1_1_i_scene.html#a178e3d8d723e7249d523a6c1d340d98b", null ],
    [ "getAllObjects", "structofbx_1_1_i_scene.html#a3312c62459109b97e5f0da4360e808ad", null ],
    [ "getAnimationStack", "structofbx_1_1_i_scene.html#a3f8f5507aabe7ed88b3b2e91d4509d1e", null ],
    [ "getAnimationStackCount", "structofbx_1_1_i_scene.html#a72027a0fe9d095a9a7ea993a817dd286", null ],
    [ "getGlobalSettings", "structofbx_1_1_i_scene.html#acbff0a56b10772b580a8e6f5a81f6a7e", null ],
    [ "getMesh", "structofbx_1_1_i_scene.html#a998a45d9dbc78707824e62895735856e", null ],
    [ "getMeshCount", "structofbx_1_1_i_scene.html#a6f508fae9e0b54e25dafa2af113d6440", null ],
    [ "getRoot", "structofbx_1_1_i_scene.html#a8561fe67b035d1125230e477de5cdb16", null ],
    [ "getRootElement", "structofbx_1_1_i_scene.html#a5912e4fed9086494cee794adb06fef76", null ],
    [ "getSceneFrameRate", "structofbx_1_1_i_scene.html#ae4ee8447f0ccf831edc15eb7766907c6", null ],
    [ "getTakeInfo", "structofbx_1_1_i_scene.html#afa34f920458b9a9a18087ab1fe4a554f", null ]
];