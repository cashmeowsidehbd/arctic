var structarctic_1_1easy_1_1_wave_header =
[
    [ "ascii", "structarctic_1_1easy_1_1_wave_header.html#a9ace57b7a180d1341221f404506c4234", null ],
    [ "audio_format", "structarctic_1_1easy_1_1_wave_header.html#a1ee69669b1a295f34321aec2e740a393", null ],
    [ "bits_per_sample", "structarctic_1_1easy_1_1_wave_header.html#a2a7f40503458d92aa56a69ea3654fda2", null ],
    [ "block_align", "structarctic_1_1easy_1_1_wave_header.html#ad2ce5e4ba4244369554fdd6f9e2c2356", null ],
    [ "byte_rate", "structarctic_1_1easy_1_1_wave_header.html#a79ef2dcd1633a80d820b44c411cf279b", null ],
    [ "channels", "structarctic_1_1easy_1_1_wave_header.html#a244ea95d65d83033306fbc8f163913cf", null ],
    [ "chunk_id", "structarctic_1_1easy_1_1_wave_header.html#acc44fe6719365802f7a5c63e80ac2a00", null ],
    [ "chunk_size", "structarctic_1_1easy_1_1_wave_header.html#a9b9f8679b39c6f82b3f68f8d74e47cbe", null ],
    [ "format", "structarctic_1_1easy_1_1_wave_header.html#a937c2266b6523ee24ce7626306fd657f", null ],
    [ "raw", "structarctic_1_1easy_1_1_wave_header.html#a4afcc2316093d52fd26a1fbc3b84250f", null ],
    [ "sample_rate", "structarctic_1_1easy_1_1_wave_header.html#a12918138061670f1d3246ee29b420b75", null ],
    [ "subchunk_1_id", "structarctic_1_1easy_1_1_wave_header.html#a0c4d785f8d8056b155570e5744141ac4", null ],
    [ "subchunk_1_size", "structarctic_1_1easy_1_1_wave_header.html#aeea66cf677599caa117bad21fd296cdc", null ],
    [ "subchunk_2_id", "structarctic_1_1easy_1_1_wave_header.html#a3c58b6df83039bf882deadeadf7f5f6f", null ],
    [ "subchunk_2_size", "structarctic_1_1easy_1_1_wave_header.html#a54021c3942908c009fd6f1e949f59b95", null ]
];