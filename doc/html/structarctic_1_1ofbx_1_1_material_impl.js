var structarctic_1_1ofbx_1_1_material_impl =
[
    [ "MaterialImpl", "structarctic_1_1ofbx_1_1_material_impl.html#a653a6482efdb85da6bc4b16c24e68e9c", null ],
    [ "getDiffuseColor", "structarctic_1_1ofbx_1_1_material_impl.html#af7e524aef21c2904dd6a316f78e79495", null ],
    [ "getTexture", "structarctic_1_1ofbx_1_1_material_impl.html#abfd3c7d97c46c057dbfa03f49a829136", null ],
    [ "getType", "structarctic_1_1ofbx_1_1_material_impl.html#a05a63ec1d2193ba7cb926cf6c4cd846d", null ],
    [ "diffuse_color", "structarctic_1_1ofbx_1_1_material_impl.html#a794d8a4a1293ea15317f7721c531e613", null ],
    [ "textures", "structarctic_1_1ofbx_1_1_material_impl.html#a8eaa1ae1a89519fba73ad1cb2c903d50", null ]
];