var structmz__zip__archive =
[
    [ "m_archive_size", "structmz__zip__archive.html#a45a402e348cc83629be5453441878d81", null ],
    [ "m_central_directory_file_ofs", "structmz__zip__archive.html#a128125bc28f4d1f118fe7b9badd0f975", null ],
    [ "m_file_offset_alignment", "structmz__zip__archive.html#ad0a89dcb30f4b8fc8b1a94c9df93d0ff", null ],
    [ "m_last_error", "structmz__zip__archive.html#a1b7722292a33d849b861e23bf94f69d9", null ],
    [ "m_pAlloc", "structmz__zip__archive.html#a1ee45d07ddf4439065e62c12f7de3286", null ],
    [ "m_pAlloc_opaque", "structmz__zip__archive.html#a54c3a1be907a1e075a112ff06b191644", null ],
    [ "m_pFree", "structmz__zip__archive.html#abb6dbd66d702cdffc05a885f8448003d", null ],
    [ "m_pIO_opaque", "structmz__zip__archive.html#a477d8ad842fa3707c2330454c8173f7d", null ],
    [ "m_pNeeds_keepalive", "structmz__zip__archive.html#a943ea385742e04f7995cb54b46892213", null ],
    [ "m_pRead", "structmz__zip__archive.html#ac3df2038c7486a5df1ed51bfa3f7443c", null ],
    [ "m_pRealloc", "structmz__zip__archive.html#ac3cdd59e8bcc156954790af62fee92e8", null ],
    [ "m_pState", "structmz__zip__archive.html#a9d47a170d9f54452fcfe1152c26af40b", null ],
    [ "m_pWrite", "structmz__zip__archive.html#a999d2d2e54211bfe0606008b6597f0d5", null ],
    [ "m_total_files", "structmz__zip__archive.html#afd3d9bd1f542c57aae52c8d6fad6f387", null ],
    [ "m_zip_mode", "structmz__zip__archive.html#a3015327b3da7922ae56ae73bb99b4fae", null ],
    [ "m_zip_type", "structmz__zip__archive.html#ae63ea3648c32998d2b17cc6dc6401671", null ]
];