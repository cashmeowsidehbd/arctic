var structmz__zip__internal__state__tag =
[
    [ "m_central_dir", "structmz__zip__internal__state__tag.html#a67c1dde93587443d48ae4c54fb32757f", null ],
    [ "m_central_dir_offsets", "structmz__zip__internal__state__tag.html#a51a9aa41f7499c7e6e4c2546aa965b9c", null ],
    [ "m_file_archive_start_ofs", "structmz__zip__internal__state__tag.html#a2705cc469d5f08543d5f827f43f391ec", null ],
    [ "m_init_flags", "structmz__zip__internal__state__tag.html#aa247669a24b55b9b9e86f732ea175bce", null ],
    [ "m_mem_capacity", "structmz__zip__internal__state__tag.html#a71f011527805e3ac48f5f12cb73d2233", null ],
    [ "m_mem_size", "structmz__zip__internal__state__tag.html#a4511d7743e1a63fb78ed066f8de96466", null ],
    [ "m_pFile", "structmz__zip__internal__state__tag.html#a983d6519b4721e4fff03f5c5d2908972", null ],
    [ "m_pMem", "structmz__zip__internal__state__tag.html#ac0f2149f384cffd332635b3a5323c377", null ],
    [ "m_sorted_central_dir_offsets", "structmz__zip__internal__state__tag.html#a1e3af54224b61cb20d6ceaeb5807bc33", null ],
    [ "m_zip64", "structmz__zip__internal__state__tag.html#a9d483ae9e65542742b3f81522c8fd077", null ],
    [ "m_zip64_has_extended_info_fields", "structmz__zip__internal__state__tag.html#a0071a211627637fc049defb3f61972fe", null ]
];