var searchData=
[
  ['m_5fcentral_5fdir_5fofs',['m_central_dir_ofs',['../structmz__zip__archive__file__stat.html#a79ac549bef6a828b37e4dc72d9282d1e',1,'mz_zip_archive_file_stat']]],
  ['m_5fcomment',['m_comment',['../structmz__zip__archive__file__stat.html#afb0f2b80d59f134f6fee4c8728649119',1,'mz_zip_archive_file_stat']]],
  ['m_5fcomment_5fsize',['m_comment_size',['../structmz__zip__archive__file__stat.html#aacf4d95d27894acac1303f51542c51b6',1,'mz_zip_archive_file_stat']]],
  ['m_5fcomp_5fsize',['m_comp_size',['../structmz__zip__archive__file__stat.html#ae2ac087fa11585219308de1677000ab3',1,'mz_zip_archive_file_stat']]],
  ['m_5fcrc32',['m_crc32',['../structmz__zip__archive__file__stat.html#af9fe260b684c70c1725a8b100cbd97a8',1,'mz_zip_archive_file_stat']]],
  ['m_5ffile_5findex',['m_file_index',['../structmz__zip__archive__file__stat.html#a9a7f0737f031b23a29c58fc7ff2bd131',1,'mz_zip_archive_file_stat']]],
  ['m_5ffilename',['m_filename',['../structmz__zip__archive__file__stat.html#ae500a60c1ca2d56199fa06f54036abea',1,'mz_zip_archive_file_stat']]],
  ['m_5finternal_5fattr',['m_internal_attr',['../structmz__zip__archive__file__stat.html#ad628219b167bef01305e8fa09a1f10d1',1,'mz_zip_archive_file_stat']]],
  ['m_5fis_5fdirectory',['m_is_directory',['../structmz__zip__archive__file__stat.html#a02509653cc5553e63a1053802f808cbc',1,'mz_zip_archive_file_stat']]],
  ['m_5fis_5fencrypted',['m_is_encrypted',['../structmz__zip__archive__file__stat.html#a952eb97b58881df827ce3808071e7754',1,'mz_zip_archive_file_stat']]],
  ['m_5fis_5fsupported',['m_is_supported',['../structmz__zip__archive__file__stat.html#a4742f25dec4e490bedc1262b96e49ed6',1,'mz_zip_archive_file_stat']]],
  ['m_5flocal_5fheader_5fofs',['m_local_header_ofs',['../structmz__zip__archive__file__stat.html#acc27b6ca5dd7159c19bc3dc32e844ac7',1,'mz_zip_archive_file_stat']]],
  ['m_5funcomp_5fsize',['m_uncomp_size',['../structmz__zip__archive__file__stat.html#aa55ee3580a844400d4bd43be27ae3808',1,'mz_zip_archive_file_stat']]],
  ['m_5fversion_5fmade_5fby',['m_version_made_by',['../structmz__zip__archive__file__stat.html#a3038faf9c1f24f2bf48eab9cc2b8ce8c',1,'mz_zip_archive_file_stat']]],
  ['msg',['msg',['../structmz__stream__s.html#ad4095b25455e382787dc06d20157e05f',1,'mz_stream_s']]]
];
