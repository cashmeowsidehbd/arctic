var structmz__zip__archive__file__stat =
[
    [ "m_bit_flag", "structmz__zip__archive__file__stat.html#ac9fdb10d21124d44be05ef0c04c1ad15", null ],
    [ "m_central_dir_ofs", "structmz__zip__archive__file__stat.html#a79ac549bef6a828b37e4dc72d9282d1e", null ],
    [ "m_comment", "structmz__zip__archive__file__stat.html#afb0f2b80d59f134f6fee4c8728649119", null ],
    [ "m_comment_size", "structmz__zip__archive__file__stat.html#aacf4d95d27894acac1303f51542c51b6", null ],
    [ "m_comp_size", "structmz__zip__archive__file__stat.html#ae2ac087fa11585219308de1677000ab3", null ],
    [ "m_crc32", "structmz__zip__archive__file__stat.html#af9fe260b684c70c1725a8b100cbd97a8", null ],
    [ "m_external_attr", "structmz__zip__archive__file__stat.html#a0303a6779c89c31eecf62b4744913272", null ],
    [ "m_file_index", "structmz__zip__archive__file__stat.html#a9a7f0737f031b23a29c58fc7ff2bd131", null ],
    [ "m_filename", "structmz__zip__archive__file__stat.html#ae500a60c1ca2d56199fa06f54036abea", null ],
    [ "m_internal_attr", "structmz__zip__archive__file__stat.html#ad628219b167bef01305e8fa09a1f10d1", null ],
    [ "m_is_directory", "structmz__zip__archive__file__stat.html#a02509653cc5553e63a1053802f808cbc", null ],
    [ "m_is_encrypted", "structmz__zip__archive__file__stat.html#a952eb97b58881df827ce3808071e7754", null ],
    [ "m_is_supported", "structmz__zip__archive__file__stat.html#a4742f25dec4e490bedc1262b96e49ed6", null ],
    [ "m_local_header_ofs", "structmz__zip__archive__file__stat.html#acc27b6ca5dd7159c19bc3dc32e844ac7", null ],
    [ "m_method", "structmz__zip__archive__file__stat.html#ae1a9a8fbc09a4c3c14b04667d0f2d189", null ],
    [ "m_time", "structmz__zip__archive__file__stat.html#ad9da17b1b5d7c66be26257c98f2492a9", null ],
    [ "m_uncomp_size", "structmz__zip__archive__file__stat.html#aa55ee3580a844400d4bd43be27ae3808", null ],
    [ "m_version_made_by", "structmz__zip__archive__file__stat.html#a3038faf9c1f24f2bf48eab9cc2b8ce8c", null ],
    [ "m_version_needed", "structmz__zip__archive__file__stat.html#a0cfb77699313b855dcd1b5bed9dfb93a", null ]
];